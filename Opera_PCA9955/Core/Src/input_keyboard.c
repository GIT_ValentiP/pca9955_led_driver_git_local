/*
 ******************************************************************************
 *  @file      : input_keyboard.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 22 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "input_keyboard.h"
#include "globals.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

GPIO_TypeDef* switch_port[NUM_MAX_SWITCHES]={SW1_LEFT_T1_GPIO_Port,SW1_CENTER_T2_GPIO_Port ,SW1_RIGHT_T3_GPIO_Port,SW2_LEFT_T4_GPIO_Port,SW2_CENTER_T5_GPIO_Port ,SW2_RIGHT_T6_GPIO_Port,SW3_LEFT_T7_GPIO_Port,SW3_CENTER_T8_GPIO_Port ,SW3_RIGHT_T9_GPIO_Port,SW4_LEFT_T10_GPIO_Port,SW4_CENTER_T11_GPIO_Port ,SW4_RIGHT_T12_GPIO_Port};
uint16_t      switch_pin[NUM_MAX_SWITCHES] ={SW1_LEFT_T1_Pin,SW1_CENTER_T2_Pin ,SW1_RIGHT_T3_Pin,SW2_LEFT_T4_Pin,SW2_CENTER_T5_Pin ,SW2_RIGHT_T6_Pin,SW3_LEFT_T7_Pin,SW3_CENTER_T8_Pin ,SW3_RIGHT_T9_Pin,SW4_LEFT_T10_Pin,SW4_CENTER_T11_Pin ,SW4_RIGHT_T12_Pin};
/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/


uint16_t sw_debounce1_counter[NUM_MAX_SWITCHES];
uint16_t sw_debounce2_counter[NUM_MAX_SWITCHES];
uint16_t sw_debounce3_counter[NUM_MAX_SWITCHES];

uint16_t switches_bitmap_deb1;
uint16_t switches_bitmap_deb2;
uint16_t switches_bitmap_deb3;

/* Function prototypes -----------------------------------------------*/
/**
 * @brief  Initialize function for input keyboard switches
 * @note   This function inizialize all variables to manage inputs switches
 * @param  None
 * @retval None
 */
void init_input_reayboard(void){
unsigned char i;
	for(i=0;i<NUM_MAX_SWITCHES;i++){
		sw_debounce1_counter[i]=0;
		sw_debounce2_counter[i]=0;
		sw_debounce3_counter[i]=0;
		sw_deb_release_cnt[i]=0;
	}

	switches_bitmap_actual=0x0000;
	switches_bitmap_deb1=0x0000;
	switches_bitmap_deb2=0x0000;
	switches_bitmap_deb3=0x0000;

	if (new_data_rx==false){
	 interclik_time_ms=500;
	 longpress_time_ms=DEBOUNCE_SW_REF1_ms;//default value
	}

	long_press_time=(longpress_time_ms/10);
	click_double_press_time=(interclik_time_ms/10);

}



/**
 * @brief  Read and debounce function.
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void read_input_keyboard(void){
uint16_t mask_bit,mask_bit_not;
unsigned char id_btn;
GPIO_PinState status_btn;
//unsigned char status_btn_old;
GPIO_TypeDef *sw_port;
mask_bit=0x0001;
mask_bit_not=~(mask_bit);


for(id_btn=0;id_btn < NUM_MAX_SWITCHES;id_btn++){
	  mask_bit=0x0001;
	  mask_bit <<=id_btn;
	  mask_bit_not=~(mask_bit);
	 // status_btn_old =((switches_bitmap_actual & mask_bit)>>id_btn);
	  sw_port=switch_port[id_btn];
	  status_btn= HAL_GPIO_ReadPin(sw_port, switch_pin[id_btn]);
      if (status_btn == GPIO_PIN_RESET){// button pressed
    	  switches_bitmap_actual |=(mask_bit);
    	 /* if (status_btn_old == 0){//first time id_btn pressed
    	  }else{ }*/
    		  sw_debounce1_counter[id_btn]++;//for distinguish long press

    		  if (sw_debounce1_counter[id_btn] >  long_press_time ){//override debounce 1
    			  sw_debounce1_counter[id_btn]= long_press_time ;
    			  switches_bitmap_deb1 |=(mask_bit);
    			  keyboard_long.bitmap |=(mask_bit);       //register long press
    			  keyboard_short.bitmap  &=(mask_bit_not); //unregister old short press
    		  }
//    		  sw_debounce2_counter[id_btn]++;
//    		  sw_debounce3_counter[id_btn]++;
//    		  if (sw_debounce2_counter[id_btn] >  middle_press_time ){//override debounce 1
//				  sw_debounce2_counter[id_btn]= middle_press_time ;
//				  switches_bitmap_deb2 |=(mask_bit);
//			  }
    		  if ((switches_bitmap_deb2 & mask_bit)!=0){//start double click
    		      switches_bitmap_deb3 |=(mask_bit);
    		      switches_bitmap_deb2 &=(mask_bit_not);
    		     //  sw_debounce2_counter[id_btn]=0;//double/single click counter
    		  }


    		  sw_deb_release_cnt[id_btn]=0;

      }else{// button id_btn released or not pressed
             /*Single or Double click*/
    	     sw_debounce2_counter[id_btn]++;
    	     if ((switches_bitmap_deb3 & mask_bit)!=0){//release of 2nd click
    	    	  //release of double click
						  keyboard_2click.bitmap |=(mask_bit); //register double click
						  keyboard_1click.bitmap  &=(mask_bit_not); //unregister possible old single click
						  switches_bitmap_deb2 &=(mask_bit_not);
						  switches_bitmap_deb3 &=(mask_bit_not);
						  sw_debounce2_counter[id_btn]=0;
    	      }else if ((switches_bitmap_actual & mask_bit)!=0){//release of first click
					   switches_bitmap_deb2 |=(mask_bit);//register first click
					   sw_debounce2_counter[id_btn]=0;//restart double/single click counter
					   keyboard_1click.bitmap  &=(mask_bit_not);
					   keyboard_2click.bitmap  &=(mask_bit_not);
					 }else if ((switches_bitmap_deb2 & mask_bit)!=0){//check if only 1 click
						       if(sw_debounce2_counter[id_btn] >click_double_press_time){//first click override interclick maximum time
								  //single click
								   keyboard_1click.bitmap |=(mask_bit); //register single click
								   keyboard_2click.bitmap  &=(mask_bit_not);//unregister possible old double click
								   switches_bitmap_deb2 &=(mask_bit_not);
								   switches_bitmap_deb3 &=(mask_bit_not);
								   sw_debounce2_counter[id_btn]=0;
						       }
					       }else{
						   //switches_bitmap_deb2 &=(mask_bit_not);
						   sw_debounce2_counter[id_btn]=0;     //
						   keyboard_1click.bitmap  &=(mask_bit_not);
						   keyboard_2click.bitmap  &=(mask_bit_not);
						  }

		     /*Short or long press*/
		     sw_debounce1_counter[id_btn]=0;//long/short press counter
			 if ((switches_bitmap_deb1 & mask_bit)!=0){//long press time elapsed
				  //keyboard_long.bitmap |=(mask_bit);
				  //keyboard_short.bitmap  &=(mask_bit_not); //unregister old short press
			 }else if ((switches_bitmap_actual & mask_bit)!=0){//short press time elapsed
				       keyboard_short.bitmap |=(mask_bit);
				       keyboard_long.bitmap  &=(mask_bit_not); //unregister old long press
			       }else{
			    	     keyboard_short.bitmap  &=(mask_bit_not); //unregister old short press
			    	     keyboard_long.bitmap  &=(mask_bit_not); //unregister old long press
                        }
		     switches_bitmap_deb1  &=(mask_bit_not);

		     /*Bitmap raw data follow physical signal at switch pin*/
    	     switches_bitmap_actual &=(mask_bit_not);//release bit of id_btn in bitmap

    	     /*Debounce also for release*/
    	    sw_deb_release_cnt[id_btn]++;
		    if(sw_deb_release_cnt[id_btn]==(DEBOUNCE_SW_RELEASE_ms)){//override debounce of released status,button return to idle 0 state
				  sw_deb_release_cnt[id_btn]=(DEBOUNCE_SW_RELEASE_ms);
			  }else if(sw_deb_release_cnt[id_btn]>(DEBOUNCE_SW_RELEASE_ms)){
				  sw_deb_release_cnt[id_btn]=(DEBOUNCE_SW_RELEASE_ms);
			}//end Debounce_release
      }//end release
 }//end_for

keyboard_raw.bitmap= switches_bitmap_actual ;

}
/**
 * @brief  Indicate the switch pressed
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void Opera_keyboard_function_7Tasti(void){


if (keyboard_dose.sw.dose1 == NO_KEY_PRESSED){
	if (keyboard_short.sw.sw1_l==1){
		keyboard_dose.sw.dose1 =LEFT_SINGLE_KEY_PRESSED;
	}else if (keyboard_short.sw.sw1_r==1){
		keyboard_dose.sw.dose1 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw1_r==1)||(keyboard_long.sw.sw1_l==1)){
		keyboard_dose.sw.dose1 =KEY_LONG_PRESSED;
	}
}

if (keyboard_dose.sw.dose2 == NO_KEY_PRESSED){
	if (keyboard_short.sw.sw2_l==1){
		keyboard_dose.sw.dose2 =LEFT_SINGLE_KEY_PRESSED;
	}else if (keyboard_short.sw.sw2_r==1){
		keyboard_dose.sw.dose2 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw2_r==1)||(keyboard_long.sw.sw2_l==1)){
		keyboard_dose.sw.dose2 =KEY_LONG_PRESSED;
	}
}
if (keyboard_dose.sw.dose3 == NO_KEY_PRESSED){
	if (keyboard_short.sw.sw3_l==1){
		keyboard_dose.sw.dose3 =LEFT_SINGLE_KEY_PRESSED;
	}else if (keyboard_short.sw.sw3_r==1){
		keyboard_dose.sw.dose3 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw3_r==1)||(keyboard_long.sw.sw3_l==1)){
		keyboard_dose.sw.dose3 =KEY_LONG_PRESSED;
	}
}
if (keyboard_dose.sw.dose4 == NO_KEY_PRESSED){
	if (keyboard_short.sw.sw4_l==1){
		keyboard_dose.sw.dose4 =LEFT_SINGLE_KEY_PRESSED;
	}else if (keyboard_short.sw.sw4_r==1){
		keyboard_dose.sw.dose4 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw4_r==1)||(keyboard_long.sw.sw4_l==1)){
		keyboard_dose.sw.dose4 =KEY_LONG_PRESSED;
	}
}

}


/**
 * @brief  Indicate the switch pressed
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
void Opera_keyboard_function_4Tasti(void){

if (keyboard_dose.sw.dose1 == NO_KEY_PRESSED){
	if  ((keyboard_1click.sw.sw1_r==1)||((keyboard_1click.sw.sw1_l==1))){
		keyboard_dose.sw.dose1 =LEFT_SINGLE_KEY_PRESSED;
	}else if ((keyboard_2click.sw.sw1_r==1)||(keyboard_2click.sw.sw1_l==1)){
		keyboard_dose.sw.dose1 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw1_r==1)||(keyboard_long.sw.sw1_l==1)){
		keyboard_dose.sw.dose1 =KEY_LONG_PRESSED;
	}
}

if (keyboard_dose.sw.dose2 == NO_KEY_PRESSED){
	if  ((keyboard_1click.sw.sw2_r==1)||((keyboard_1click.sw.sw2_l==1))){
		keyboard_dose.sw.dose2 =LEFT_SINGLE_KEY_PRESSED;
	}else if ((keyboard_2click.sw.sw2_r==1)||(keyboard_2click.sw.sw2_l==1)){
		keyboard_dose.sw.dose2 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw2_r==1)||(keyboard_long.sw.sw2_l==1)){
		keyboard_dose.sw.dose2 =KEY_LONG_PRESSED;
	}
}

if (keyboard_dose.sw.dose3 == NO_KEY_PRESSED){
	if  ((keyboard_1click.sw.sw3_r==1)||((keyboard_1click.sw.sw3_l==1))){
		keyboard_dose.sw.dose3 =LEFT_SINGLE_KEY_PRESSED;
	}else if ((keyboard_2click.sw.sw3_r==1)||(keyboard_2click.sw.sw3_l==1)){
		keyboard_dose.sw.dose3 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw3_r==1)||(keyboard_long.sw.sw3_l==1)){
		keyboard_dose.sw.dose3 =KEY_LONG_PRESSED;
	}
}

if (keyboard_dose.sw.dose4 == NO_KEY_PRESSED){
	if  ((keyboard_1click.sw.sw4_r==1)||((keyboard_1click.sw.sw4_l==1))){
		keyboard_dose.sw.dose4 =LEFT_SINGLE_KEY_PRESSED;
	}else if ((keyboard_2click.sw.sw4_r==1)||(keyboard_2click.sw.sw4_l==1)){
		keyboard_dose.sw.dose4 =RIGHT_DOUBLE_PRESSED;
	}else if ((keyboard_long.sw.sw4_r==1)||(keyboard_long.sw.sw4_l==1)){
		keyboard_dose.sw.dose4 =KEY_LONG_PRESSED;
	}
}




}
/**
 * @brief  Indicate the switch pressed
 * @note   This function have to be called in.
 * @param  None
 * @retval None
 */
unsigned char keyboard_function_manager(void){
unsigned char ret=0;
	if (keyboard_raw.sw.sw1_l==1){
		ret=1;
	}
	if (keyboard_raw.sw.sw1_c==1){
			ret=2;
    }
	if (keyboard_raw.sw.sw1_r==1){
			ret=3;
	}
	if (keyboard_raw.sw.sw2_l==1){
			ret=4;
		}
	if (keyboard_raw.sw.sw2_c==1){
			ret=5;
		}
	if (keyboard_raw.sw.sw2_r==1){
			ret=6;
		}
	if (keyboard_raw.sw.sw3_l==1){
			ret=7;
		}
	if (keyboard_raw.sw.sw3_c==1){
			ret=8;
		}
	if (keyboard_raw.sw.sw3_r==1){
			ret=9;
		}
	if (keyboard_raw.sw.sw4_l==1){
			ret=10;
		}
	if (keyboard_raw.sw.sw4_c==1){
			ret=11;
		}
	if (keyboard_raw.sw.sw4_r==1){
			ret=12;
		}
return (ret);

}
