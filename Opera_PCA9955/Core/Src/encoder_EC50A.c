/*
 ******************************************************************************
 *  @file      : encoder_EC50A.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 19 lug 2019
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : SANREMO_Opera 
 *   
 *  @brief     : Cortex-M0 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
//#include "encoder_EC50A.h"
//#include "main.h"
#include "gpio.h"
/* Private includes ----------------------------------------------------------*/
#include "globals.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Return with status macro */
#define RETURN_WITH_STATUS(p, s)    (p)->Rotation = s; return s
/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/



ENCODER_Rotate_t Encoder_Get(ENCODER_TYPE_t* data) {
	/* Calculate everything */
		data->Diff = data->Enc_Count - data->Absolute;
		data->Absolute = (data->Enc_Count);
	    data->Detent = (data->Enc_Count % MAX_ENC_DETENT);
	    data->LastDetentRead += (int16_t)data->Diff;
		/* Check */
		if (data->LastDetentRead < 0){//(data->Diff < 0) {
			RETURN_WITH_STATUS(data, Enc_Rotate_Decrement);
		} else if (data->LastDetentRead > 0) {
			RETURN_WITH_STATUS(data, Enc_Rotate_Increment);
		}

		RETURN_WITH_STATUS(data, Enc_Rotate_Nothing);
}
void Encoder_Init(void) {
		/* Set mode */

		//data = malloc(sizeof(ENCODER_TYPE_t));
		ENCODER_default.Absolute=0;
		ENCODER_default.Diff=0;
		ENCODER_default.Rotation=Enc_Rotate_Nothing;
		ENCODER_default.Mode=Encoder_Mode_1;
		ENCODER_default.LastA=0;
		ENCODER_default.LastEncoded=0;
		ENCODER_default.Enc_Count=0;
		ENCODER_default.LastDetentRead=0;
		encoder_data=&ENCODER_default;
	//	encoder_data->Mode = mode;
	//	encoder_data->Rotation=Enc_Rotate_Nothing;//.Mode=Encoder_Mode_0
	//	encoder_data->Diff = 0;
	//	encoder_data->Absolute =0;
	//	encoder_data->LastA=0;
	//	encoder_data->Enc_Count = 0;
}

void Encoder_SetMode(ENCODER_TYPE_t* data, ENCODER_Mode_t mode) {
	/* Set mode */

	//data = malloc(sizeof(ENCODER_TYPE_t));

	data->Mode = mode;
//	encoder_data->Rotation=Enc_Rotate_Nothing;//.Mode=Encoder_Mode_0
//	encoder_data->Diff = 0;
//	encoder_data->Absolute =0;
//	encoder_data->LastA=0;
//	encoder_data->Enc_Count = 0;
}

void Encoder_Process(ENCODER_TYPE_t* data) {
	GPIO_PinState enc_cha_status;
	uint8_t now_a;
	GPIO_PinState enc_chb_status;
	uint8_t now_b;
	uint8_t encoded=0;
	uint8_t sum=0;
//	unsigned int iter1;  /*delay*/
//	for(iter1=0;iter1 < 100;iter1++){
//			__NOP();
//    }

	/* Read inputs */
	enc_cha_status =  HAL_GPIO_ReadPin(ENCODER_CHA_GPIO_Port, ENCODER_CHA_Pin);//TM_GPIO_GetInputPinValue(data->GPIO_A, data->GPIO_PIN_A);
	enc_chb_status =  HAL_GPIO_ReadPin(ENCODER_CHB_GPIO_Port, ENCODER_CHB_Pin);//TM_GPIO_GetInputPinValue(data->GPIO_B, data->GPIO_PIN_B);

	now_a=READ_PIN_STATUS(enc_cha_status);
	now_b=READ_PIN_STATUS(enc_chb_status);

  encoded = (now_a << 1) | now_b; //converting the 2 pin value to single number
	if (data->LastEncoded ==0){
		if (now_a ==1){
			  data->LastEncoded =0;
		   }else{
				     data->LastEncoded =0;
		        }
	}
  sum  = ( data->LastEncoded  << 2) | encoded; //adding it to the previous encoded value

	if (now_a != data->LastA) {
			if((sum == 0x0D)|| (sum == 0x04)|| (sum == 0x02) || (sum == 0x0B)) {
				 data->Enc_Count++;// encoderValue ++;
				}
			if((sum == 0x0E) || (sum == 0x07) || (sum == 0x01) || (sum == 0x08)) {
				data->Enc_Count--;//encoderValue --;
			}
			data->LastA  = now_a; //store

  }
	data->LastEncoded = encoded; //store this value for next time
	/* Check difference */
//	if (now_a != data->LastA) {
//		data->LastA = now_a;

//		if (data->LastA == 0) {
//			/* Check mode */
//			if (data->Mode == Encoder_Mode_0) {
//				if (now_b == 1) {
//					data->Enc_Count--;
//				} else {
//					data->Enc_Count++;
//				}
//			} else {
//				if (now_b == 1) {
//					data->Enc_Count++;
//				} else {
//					data->Enc_Count--;
//				}
//			}
//		}
//	}
}
