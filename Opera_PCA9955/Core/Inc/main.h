/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "encoder_EC50A.h"
#include "input_keyboard.h"
#include "led_driver_PCA9685.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

// types
typedef struct system_timer_s
{
	double tick_10ms;              //
    double tick_100ms;            //
    double seconds;               //
    double mins;                  //
    double hours;                 //
} sys_timer_t;


typedef union{
	struct int_bit_s{
			uint16_t sw1_l:1;
			uint16_t sw1_c:1;
			uint16_t sw1_r:1;
			uint16_t sw2_l:1;
			uint16_t sw2_c:1;
			uint16_t sw2_r:1;
			uint16_t sw3_l:1;
			uint16_t sw3_c:1;
			uint16_t sw3_r:1;
			uint16_t sw4_l:1;
			uint16_t sw4_c:1;
			uint16_t sw4_r:1;
			uint16_t sw5_l:1;
			uint16_t sw5_c:1;
			uint16_t sw5_r:1;
			uint16_t sw_free1:1;
	}sw;

	    uint16_t bitmap;
}Keys_Func_t;

typedef union{
	struct func_bit_s{
			uint16_t dose1:2;
			uint16_t dose2:2;
			uint16_t dose3:2;
			uint16_t dose4:2;
			uint16_t dose5:2;
			uint16_t dose6:2;
			uint16_t dose7:2;
			uint16_t dose8:2;
	}sw;

	    uint16_t bitmap;
}Keys_Func_Opera_t;





typedef union{
	struct byte_bit_s{
			uint8_t b0:1;
			uint8_t b1:1;
			uint8_t b2:1;
			uint8_t b3:1;
			uint8_t b4:1;
			uint8_t b5:1;
			uint8_t b6:1;
			uint8_t b7:1;
	}bit;
	    uint8_t bytes;
}byte_bit_t;

typedef struct {
			uint16_t duty_RED;
			uint16_t duty_GREEN;
			uint16_t duty_BLUE;
			uint8_t id_col;
}RGB_LED_t;

typedef enum {
			NOTHING_TO_READ,
			ENCODER_READ,
			KEYBOARD_BITMAP_READ,
			KEYBOARD_LOGIC_READ,
			FW_REL_READ,
			OPERA_STATUS_READ
}RS485_PARAM_READ_enum_t;

typedef enum {
			OPERA_NOT_CONFIG=0x0000,
			OPERA_READY=0x0001,
			OPERA_RESET_TO_DEFAULT=0x0002,
			OPERA_ERROR=0x0003
}OPERA_STATUS_enum_t;

typedef enum {
			OPERA_4_TASTI=0x0000,
			OPERA_7_TASTI=0x0001,
			OPERA_NOT_SET=0xFFFF
}OPERA_CONFIG_enum_t;


/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
extern GPIO_TypeDef* switch_port[];




/* USER CODE END EC */


/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

#define BIT_0         (0x01U)
#define BIT_1         (0x02U)
#define BIT_2         (0x04U)
#define BIT_3         (0x08U)
#define BIT_4         (0x10U)
#define BIT_5         (0x20U)
#define BIT_6         (0x40U)
#define BIT_7         (0x80U)

#define BIT_8         (0x0100U)
#define BIT_9         (0x0200U)
#define BIT_10        (0x0400U)
#define BIT_11        (0x0800U)
#define BIT_12        (0x1000U)
#define BIT_13        (0x2000U)
#define BIT_14        (0x4000U)
#define BIT_15        (0x8000U)

#define BIT_16        (0x010000U)
#define BIT_17        (0x020000U)
#define BIT_18        (0x040000U)
#define BIT_19        (0x080000U)
#define BIT_20        (0x100000U)
#define BIT_21        (0x200000U)
#define BIT_22        (0x400000U)
#define BIT_23        (0x800000U)

#define BIT_24        (0x01000000U)
#define BIT_25        (0x02000000U)
#define BIT_26        (0x04000000U)
#define BIT_27        (0x08000000U)
#define BIT_28        (0x10000000U)
#define BIT_29        (0x20000000U)
#define BIT_30        (0x40000000U)
#define BIT_31        (0x80000000U)
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
/*TIMERS CONSTANTS DEFINE*/
#define TIMER6_RELOAD_TO_10us                       65525
#define TIMER6_PRESCALER_1MHz                          16
#define COUNT_TO_1ms                                  100
#define COUNT_TO_100ms                              10000
#define TIMER7_RELOAD_TO_10ms                       55535
#define OSC_EXT_FREQ                             16000000

/*RS485  DEFINE*/
#define OPERA_RS485_ADDR                               16//18//17//16
#define RS485_BAUDRATE                              38400
#define RS485_CMD_RD_TASTI                             32

/*RS485 OPERA PROTOCOL REGISTERS*/
#define RS485_NUM_BYTES                                 5
#define RS485_ADR_POS                                   0
#define RS485_CMD_POS                                   1
#define RS485_DATA1_POS                                 2
#define RS485_CRC_LOW_POS                               3
#define RS485_CRC_HI_POS                                4
#define RS485_SLAVE_ADR                               0x10
/*READ REGISTERS*/
/*STATUS DOSE BUTTON  Command 0x20*/
#define SW1_RS485_SEND_MSK                     0b00000001    /* SINGOLA PRESSIONE PULSANTE DOSE1   */
#define SW1_DP_RS485_SEND_MSK                  0b00000010    /* DOPPIA PRESSIONE PULSANTE DOSE1    */
#define SW2_RS485_SEND_MSK                     0b00000100    /* SINGOLA PRESSIONE PULSANTE DOSE1   */
#define SW2_DP_RS485_SEND_MSK                  0b00001000    /* DOPPIA PRESSIONE PULSANTE DOSE1    */
#define SW3_RS485_SEND_MSK                     0b00010000    /* SINGOLA PRESSIONE PULSANTE DOSE1   */
#define SW3_DP_RS485_SEND_MSK                  0b00100000    /* DOPPIA PRESSIONE PULSANTE DOSE1    */
#define SW4_RS485_SEND_MSK                     0b01000000    /* SINGOLA PRESSIONE PULSANTE DOSE1   */
#define SW4_DP_RS485_SEND_MSK                  0b10000000    /* DOPPIA PRESSIONE PULSANTE DOSE1    */

/*STATUS ENCODER*/
#define ENC_ROT_ANTICW_RS485_MSG                     1    /*COUNTER CLOCK WISE ROTATIO*/
#define ENC_ROT_CW_RS485_MSG                         0    /*CLOCK WISE ROTATION*/
#define ENC_ROT_RS485_MSK                   0b00000001    /* Posizione informazione ROTAZIONE ENCODER   */
#define ENC_DETENT_RS485_MSK                0b00011110    /* SINGOLA PRESSIONE PULSANTE DOSE1   */


/*WRITE REGISTERS*/
/*DOSE 1,2,3 e MANUALE*/
#define LED_DOSE_ON                      1    /**/
#define LED_DOSE_OFF                     0    /**/

#define COL_LED_DOSE_RS485_MSK                0b00001110    /* MASK DATA BYTE for color of RGB led from 1 to 7  */




/*GPIO LABELS*/

/*SWITCHes LABELS*/

#define SW1_LEFT_T1_Pin 					   GPIO_PIN_2
#define SW1_LEFT_T1_GPIO_Port 						GPIOC
#define SW1_CENTER_T2_Pin                      GPIO_PIN_1
#define SW1_CENTER_T2_GPIO_Port 					GPIOC
#define SW1_RIGHT_T3_Pin                       GPIO_PIN_0
#define SW1_RIGHT_T3_GPIO_Port                      GPIOC


#define SW2_LEFT_T4_Pin                       GPIO_PIN_15
#define SW2_LEFT_T4_GPIO_Port                       GPIOC
#define SW2_CENTER_T5_Pin                     GPIO_PIN_14
#define SW2_CENTER_T5_GPIO_Port                     GPIOC
#define SW2_RIGHT_T6_Pin                      GPIO_PIN_13
#define SW2_RIGHT_T6_GPIO_Port                      GPIOC

#define SW3_LEFT_T7_Pin                       GPIO_PIN_10
#define SW3_LEFT_T7_GPIO_Port                       GPIOC
#define SW3_CENTER_T8_Pin                     GPIO_PIN_11
#define SW3_CENTER_T8_GPIO_Port                     GPIOC
#define SW3_RIGHT_T9_Pin                       GPIO_PIN_12
#define SW3_RIGHT_T9_GPIO_Port                       GPIOC

#define SW4_LEFT_T10_Pin                       GPIO_PIN_2
#define SW4_LEFT_T10_GPIO_Port                      GPIOD
#define SW4_CENTER_T11_Pin                     GPIO_PIN_3
#define SW4_CENTER_T11_GPIO_Port                    GPIOB
#define SW4_RIGHT_T12_Pin                      GPIO_PIN_4
#define SW4_RIGHT_T12_GPIO_Port                     GPIOB


/*RS485  UART2  LABELS*/
#define RS485_RE_UART2CTS_Pin 				   GPIO_PIN_0
#define RS485_RE_UART2CTS_GPIO_Port 				GPIOA
#define RS485_DE_UART2RTS_Pin 				   GPIO_PIN_1
#define RS485_DE_UART2RTS_GPIO_Port 				GPIOA
#define RS485_MCU_TX_Pin 					   GPIO_PIN_2
#define RS485_MCU_TX_GPIO_Port       				GPIOA
#define RS485_MCU_RX_Pin 					   GPIO_PIN_3
#define RS485_MCU_RX_GPIO_Port 						GPIOA


/*ENCODER LABELS*/
#define ENCODER_CHA_Pin 					   GPIO_PIN_4
#define ENCODER_CHA_GPIO_Port 						GPIOA
#define ENCODER_CHA_EXTI_IRQn               EXTI4_15_IRQn
#define ENCODER_CHB_Pin                        GPIO_PIN_5
#define ENCODER_CHB_GPIO_Port                       GPIOA
#define ENCODER_CHB_EXTI_IRQn               EXTI4_15_IRQn

/*DISPLAY 17-SEGMENTS LABELS*/
#define SEG_A1_1_Pin                           GPIO_PIN_6
#define SEG_A1_1_GPIO_Port                          GPIOA
#define SEG_A2_18_Pin                          GPIO_PIN_8
#define SEG_A2_18_GPIO_Port                         GPIOA
#define SEG_B_16_Pin                           GPIO_PIN_9
#define SEG_B_16_GPIO_Port                          GPIOC
#define SEG_C_13_Pin                           GPIO_PIN_8
#define SEG_C_13_GPIO_Port                          GPIOC
#define SEG_D1_9_Pin                           GPIO_PIN_7
#define SEG_D1_9_GPIO_Port                          GPIOC
#define SEG_D2_10_Pin                          GPIO_PIN_6
#define SEG_D2_10_GPIO_Port                         GPIOC
#define SEG_E_8_Pin                           GPIO_PIN_15
#define SEG_E_8_GPIO_Port                           GPIOB
#define SEG_F_4_Pin                            GPIO_PIN_7
#define SEG_F_4_GPIO_Port                           GPIOA
#define SEG_G1_5_Pin                          GPIO_PIN_13
#define SEG_G1_5_GPIO_Port                          GPIOB
#define SEG_G2_15_Pin                         GPIO_PIN_15
#define SEG_G2_15_GPIO_Port                         GPIOA
#define SEG_H_3_Pin                           GPIO_PIN_11
#define SEG_H_3_GPIO_Port                           GPIOB
#define SEG_J_2_Pin                            GPIO_PIN_9
#define SEG_J_2_GPIO_Port                           GPIOB
#define SEG_K_17_Pin                           GPIO_PIN_2
#define SEG_K_17_GPIO_Port                          GPIOB
#define SEG_L_14_Pin                           GPIO_PIN_1
#define SEG_L_14_GPIO_Port                          GPIOB
#define SEG_M_6_Pin                            GPIO_PIN_0
#define SEG_M_6_GPIO_Port                           GPIOB
#define SEG_N_7_Pin                            GPIO_PIN_5
#define SEG_N_7_GPIO_Port                           GPIOC
#define SEG_DP_12_Pin                          GPIO_PIN_4
#define SEG_DP_12_GPIO_Port                         GPIOC


#define SEG_A1_Pin                           GPIO_PIN_6
#define SEG_A1_GPIO_Port                          GPIOA
#define SEG_A2_Pin                           GPIO_PIN_8
#define SEG_A2_GPIO_Port                          GPIOA
#define SEG_B_Pin                            GPIO_PIN_9
#define SEG_B_GPIO_Port                          GPIOC
#define SEG_C_Pin                           GPIO_PIN_8
#define SEG_C_GPIO_Port                          GPIOC
#define SEG_D1_Pin                           GPIO_PIN_7
#define SEG_D1_GPIO_Port                          GPIOC
#define SEG_D2_Pin                          GPIO_PIN_6
#define SEG_D2_GPIO_Port                         GPIOC
#define SEG_E_Pin                           GPIO_PIN_15
#define SEG_E_GPIO_Port                           GPIOB
#define SEG_F_Pin                            GPIO_PIN_7
#define SEG_F_GPIO_Port                           GPIOA
#define SEG_G1_Pin                          GPIO_PIN_13
#define SEG_G1_GPIO_Port                          GPIOB
#define SEG_G2_Pin                         GPIO_PIN_15
#define SEG_G2_GPIO_Port                         GPIOA
#define SEG_H_Pin                           GPIO_PIN_11
#define SEG_H_GPIO_Port                           GPIOB
#define SEG_J_Pin                            GPIO_PIN_9
#define SEG_J_GPIO_Port                           GPIOB
#define SEG_K_Pin                           GPIO_PIN_2
#define SEG_K_GPIO_Port                          GPIOB
#define SEG_L_Pin                           GPIO_PIN_1
#define SEG_L_GPIO_Port                          GPIOB
#define SEG_M_Pin                            GPIO_PIN_0
#define SEG_M_GPIO_Port                           GPIOB
#define SEG_N_Pin                            GPIO_PIN_5
#define SEG_N_GPIO_Port                           GPIOC
#define SEG_DP_Pin                          GPIO_PIN_4
#define SEG_DP_GPIO_Port                         GPIOC


/* DIPSWITCH/SPI2 LABELS */

#define SPI2_MOSI_DPSW1_Pin 				   GPIO_PIN_3
#define SPI2_MOSI_DPSW1_GPIO_Port 					GPIOC
#define SPI2_NSS_DPSW2_Pin                    GPIO_PIN_12
#define SPI2_NSS_DPSW2_GPIO_Port                    GPIOB
#define SPI2_MISO_DPSW3_Pin                   GPIO_PIN_14
#define SPI2_MISO_DPSW3_GPIO_Port                   GPIOB
#define SPI2_SCK_DPSW4_Pin                    GPIO_PIN_10
#define SPI2_SCK_DPSW4_GPIO_Port                    GPIOB



/*  RS232/UART1 LABELS  */
#define RS232_TO_PC_UART1_TX_Pin               GPIO_PIN_9
#define RS232_TO_PC_UART1_TX_GPIO_Port              GPIOA
#define RS232_FROM_PC_UART1_RX_Pin            GPIO_PIN_10
#define RS232_FROM_PC_UART1_RX_GPIO_Port            GPIOA

/* JTAG - SingleWireDebug LABELS */
#define JTAG_SWDIO_Pin                        GPIO_PIN_13
#define JTAG_SWDIO_GPIO_Port                        GPIOA
#define JTAG_SWCLK_Pin                        GPIO_PIN_14
#define JTAG_SWCLK_GPIO_Port                        GPIOA

/*LED DRIVER I2C*/
#define LED_DRIVER__OE_Pin                     GPIO_PIN_5
#define LED_DRIVER__OE_GPIO_Port                    GPIOB
#define LED_DRIVER_SCL_Pin                     GPIO_PIN_6
#define LED_DRIVER_SCL_GPIO_Port                    GPIOB
#define LED_DRIVER_SDA_Pin                     GPIO_PIN_7
#define LED_DRIVER_SDA_GPIO_Port                    GPIOB


/*LED General Purpose*/
#define LED_SERVICE1_Pin                      GPIO_PIN_14
#define LED_SERVICE1_GPIO_Port                      GPIOB

#define LED_SERVICE2_Pin                      GPIO_PIN_12
#define LED_SERVICE2_GPIO_Port                      GPIOB

#define LED_SERVICE3_Pin                      GPIO_PIN_10
#define LED_SERVICE3_GPIO_Port                      GPIOB

#define LED_SERVICE4_Pin                       GPIO_PIN_3
#define LED_SERVICE4_GPIO_Port                      GPIOC

#define LED_SERVICE5_Pin                       GPIO_PIN_8
#define LED_SERVICE5_GPIO_Port                      GPIOB

/* USER CODE BEGIN Private defines */
/*TIMER 6*/

#define RS485_TX_ENABLE     HAL_GPIO_WritePin(RS485_DE_UART2RTS_GPIO_Port,RS485_DE_UART2RTS_Pin,GPIO_PIN_SET)
#define RS485_TX_DISABLE    HAL_GPIO_WritePin(RS485_DE_UART2RTS_GPIO_Port,RS485_DE_UART2RTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_ENABLE     HAL_GPIO_WritePin(RS485_RE_UART2CTS_GPIO_Port,RS485_RE_UART2CTS_Pin,GPIO_PIN_RESET)
#define RS485_RX_DISABLE    HAL_GPIO_WritePin(RS485_RE_UART2CTS_GPIO_Port,RS485_RE_UART2CTS_Pin,GPIO_PIN_SET)



/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
