/*
 * encoder_EC50A.h
 *
 *  Created on: 19 lug 2019
 *      Author: firmware
 */

#ifndef INC_ENCODER_EC50A_H_
#define INC_ENCODER_EC50A_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
/* Typedef -----------------------------------------------------------*/
/**
 * @brief  Rotary encoder rotation status
 */
typedef enum {
	Enc_Rotate_Increment, /*!< Encoder was incremented */
	Enc_Rotate_Decrement, /*!< Encoder was decremented */
	Enc_Rotate_Nothing    /*!< Encoder stop at it was before */
} ENCODER_Rotate_t;

/**
 * @brief  Rotary encoder mode selection for rotation
 */
typedef enum {
	Encoder_Mode_0, /*!< Rotary encoder mode zero. It is used for direction when it will be increment od decrement, default used */
	Encoder_Mode_1   /*!< Rotary encoder mode one. It is used for direction when it will be increment od decrement */
} ENCODER_Mode_t;

/**
 * @brief  Rotary main working structure
 */
typedef struct {
	int32_t Absolute;        /*!< Absolute rotation from beginning, for public use */
	int32_t Diff;            /*!< Rotary difference from last check, for public use */
	ENCODER_Rotate_t Rotation; /*!< Increment, Decrement or nothing, for public use */
	ENCODER_Mode_t Mode;       /*!< Rotary encoder mode selected */
	uint8_t LastA;           /*!< Last status of A pin when checking. Meant for private use */
	int32_t Enc_Count;        /*!< Temporary variable to store data between rotation and user check */
	uint8_t LastEncoded;
	uint16_t Detent;
	int16_t LastDetentRead;
//	GPIO_TypeDef* GPIO_A;    /*!< Pointer to GPIOx for Rotary encode A pin. Meant for private use */
//	GPIO_TypeDef* GPIO_B;    /*!< Pointer to GPIOx for Rotary encode B pin. Meant for private use */
//	uint16_t GPIO_PIN_A;     /*!< GPIO pin for rotary encoder A pin. This pin is also set for interrupt */
//	uint16_t GPIO_PIN_B;     /*!< GPIO pin for rotary encoder B pin. */
} ENCODER_TYPE_t;

/**
 * @}
 */

/* Define ------------------------------------------------------------*/
#define MAX_ENC_DETENT 16
/* Macro -------------------------------------------------------------*/

/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/
void encoder_read(void);


/**
 * @defgroup TM_ROTARY_ENCODER_Functions
 * @brief    Library Functions
 * @{
 */
void Encoder_Init(void);

/**
 * @brief  Set rotary encoder custom mode
 * @param  *data: Pointer to @ref ENCODER_TYPE_t structure for specific rotary encoder input
 * @param  mode: Rotary mode you will use. This parameter can be a value of @ref ENCODER_Mode_t enumeration
 * @retval None
 */
void Encoder_SetMode(ENCODER_TYPE_t* data, ENCODER_Mode_t mode);

/**
 * @brief  Checks and gets new values of rotary encoder
 * @param  *data: Pointer to @ref TM_RE_t structure
 * @retval Member of @ref TM_RE_Rotate_t
 */
ENCODER_Rotate_t Encoder_Get(ENCODER_TYPE_t* data);

/**
 * @brief  Process function.
 * @note   This function have to be called inside your interrupt handler.
 * @param  *data: Pointer to rotary encoder @ret ENCODER_TYPE_t data where interrupt occured
 * @retval None
 */
void Encoder_Process(ENCODER_TYPE_t* data);

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */








#endif /* INC_ENCODER_EC50A_H_ */
